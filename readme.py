# Python3 (tested on Python 3.6.4)

## load image ids
with open('train_ids_2075.pkl', 'rb') as f:
    train_ids = pickle.load(f) # len(train_ids) = 2075

with open('valid_ids_519.pkl', 'rb') as f:
    valid_ids = pickle.load(f) # len(valid_ids) = 519


## git command
# clone to local 
# git clone https://username@bitbucket.org/username/reponame.git
# 
# quick start with Bitbucket using git 
# 1) Create a new repository under your Bitbucket account.
# 2) Open the terminal and change directory to the one that you would like to upload.
# 3) Run following commands (please change commit notes, repo address ending with ".git", and branch name if necessary)
# git init
# git add --all                                                      
# git commit -m "init commit"
# git remote add origin https://username@bitbucket.org/username/reponame.git
# git push origin master 
# Then you should be able to find your uploaded code under "Source" on Bitbucket.org. To share a repo with others, please use "share" button at the top-right corner under "Overview". Repo URL is also available under "Overview" with HTTPS or SSH. 
#
# Some notes:
# a) Check status: "git status"
# b) Add specific file: "git add filename.extension"
# c) Add all files in current directory: "git add --all" or "git add . -A"
# d) List branches: "git branch"
# e) Create a branch named "develop" and switch to the new branch: "git checkout -b develop"
# f) Force push: "git push -f origin master " (It might be used when replacing files with existing repo)
# g) Upload code from directory with ".git":
#    git add --all                                                      
#    git commit -m "init commit"
#    git push origin master 

